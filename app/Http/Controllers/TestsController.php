<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class TestsController extends Controller
{
    public function index() {
        $hash = array(
            'test1' => 'テスト1',
            'test2' => 'テスト2',
            'test3' => 'テスト3'
        );
        return view('test.index')->with($hash);
    }

}
