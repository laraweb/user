<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class RedirectTestController extends Controller
{
    /**
     * リダイレクト元
     */
    public function from() {
        return redirect('redirecttest/to')->with('status', 'リダイレクト処理成功。Flashメッセージ表示');
    }

    /**
     * リダイレクト先
     */
    public function to() {
        return view('redirecttest.to');
    }
    
}
