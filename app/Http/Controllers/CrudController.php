<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Crud;
use App\Http\Requests\CrudRequest;


class CrudController extends Controller
{
    /**
     *
     * 一覧表示
     *
     */
    public function index(Request $request)
    {
        #キーワード受け取り
        $keyword = $request->input('keyword');

        //クエリ生成
        $query = Crud::query();

        //もしキーワードがあったら
        if(!empty($keyword))
        {
            $query->where('name','like','%'.$keyword.'%')->orWhere('mail','like','%'.$keyword.'%');
        }

        //ページネーション
        $data = $query->orderBy('created_at','desc')->paginate(5);
        return view('crud.index')->with('data',$data)
                                  ->with('keyword',$keyword)
                                  ->with('message','ユーザーリスト');
    }


    /**
     *
     * 新規作成
     *
     */
    public function create()
    {
        return view('crud.create')->with('message','登録するユーザーを入力してください。');
    }

    /**
     *
     * 新規保存
     *
     */
    public function store(CrudRequest $request)
    {

        $crud = new Crud();
        $crud->name = $request->name;
        $crud->mail = $request->mail;
        $crud->gender = $request->gender;
        $crud->age = $request->age;
        $crud->pref = $request->pref;
        $crud->birthday = $request->birthday;
        $crud->tel = $request->tel;
        $crud->save();

        # View表示
        $res = $request->input('name')."さんを追加しました。";
        $data = Crud::latest('created_at')->paginate(10);
        return redirect('/crud')->with('message',$res)->with('data',$data)->with('status','新規保存の処理完了！');
    }

    /**
     *
     * 詳細表示
     *
     */
    public function show($id)
    {
        $user = Crud::findOrFail($id);
        return view('crud.show')->with('user', $user);
    }

    /**
     *
     * 編集
     *
     */
    public function edit($id)
    {
        $data = Crud::findOrFail($id);
        return view('crud.edit')->with('message','編集フォーム')->with('data',$data);
    }

    /**
     *
     * 更新
     *
     */
    public function update(CrudRequest $request, $id)
    {
        $crud = Crud::findOrFail($id);
        $crud->name = $request->name;
        $crud->mail = $request->mail;
        $crud->gender = $request->gender;
        $crud->age = $request->age;
        $crud->pref = $request->pref;
        $crud->birthday = $request->birthday;
        $crud->tel = $request->tel;
        $crud->save();

        # View表示
        $res = $request->input('name')."さんを更新しました。";
        $data = Crud::latest('created_at')->paginate(10);
        return redirect('/crud/')->with('message',$res)->with('data',$data)->with('status','更新処理完了！');
    }

    /**
     *
     * 削除
     *
     */
    public function destroy($id)
    {
        $crud = Crud::findOrFail($id);
        $crud->delete();

        # 削除処理
        # $user = User::all();
        $data = Crud::latest('created_at')->get();
        return redirect('/crud/')->with('status', '削除処理完了！')->with('data',$data);
    }


}
