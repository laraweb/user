<?php

namespace App\Http\Controllers\AdminAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $redirectTo = '/admin/home'; //パスワード再設定後の飛び先
    protected $linkRequestView = 'adminAuth.passwords.email'; //パスワードリセット用のビューの場所を指定。
    protected $resetView = 'adminAuth.passwords.reset';
    protected $guard = 'admin'; //ガードの種別指定。
    protected $broker = 'admins'; //config/auth.php内で指定したpasswordの種別を指定。

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin'); //guestミドルウェアのガードにadminを指定
    }
}