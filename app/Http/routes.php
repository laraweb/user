<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
 *  スタートページ
 */
Route::get('/', function () {
   return view('welcome');
 });

/*
 * with/compact demo
 */
Route::get('/test/index', 'TestsController@index');

/*
 * Redirect/Flash demo
 */
Route::get('/redirecttest/from', 'RedirectTestController@from');
Route::get('/redirecttest/to', 'RedirectTestController@to');

Route::group(['middleware' => 'auth.admin'], function(){   #ベーシック認証

	/*
	 * CRUDアプリ
	 */
	#Route::resource('/user/crud', 'CrudController');
        Route::get('/crud', 'CrudController@index');
        Route::get('/crud/create', 'CrudController@create');
        Route::post('/crud', 'CrudController@store');
        Route::get('/crud/{id}', 'CrudController@show');
        Route::get('/crud/{id}/edit', 'CrudController@edit');
        Route::patch('/crud/{id}', 'CrudController@update');
        Route::delete('/crud/{id}', 'CrudController@destroy');
        
    /*
	 * メール送信
	 */
		Route::get('/mail', 'MailController@index');
		Route::post('/mail','MailController@confirm');
		Route::post('/mail/complete','MailController@complete');


       /*
        * 画像アップロード
        */
        Route::get('/photos/', 'PhotosController@index');
        Route::post('/photos/store', 'PhotosController@store');

	/*
	 * マルチ認証
	 */
	
            /*
             *  共用(ユーザー・管理者)
             */
            Route::get('/multi_auth', function () {
                return view('multi_auth');
            });
	
            /*
             *  ユーザー
             */
            #Route::auth();
            Route::get('/home', 'HomeController@index');
	
            $this->get('login', 'Auth\AuthController@showLoginForm');
            $this->post('login', 'Auth\AuthController@login');
            $this->get('logout', 'Auth\AuthController@logout');
            
            $this->get('register', 'Auth\AuthController@showRegistrationForm');
            $this->post('register', 'Auth\AuthController@register');
            
            $this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
            $this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
            $this->post('password/reset', 'Auth\PasswordController@reset');
            
	
            /*
             *  管理者
             */
            Route::get('/admin/home','AdminHomeController@index');
		
            $this->get('/admin/login', 'AdminAuth\AuthController@showLoginForm');
            $this->post('/admin/login', 'AdminAuth\AuthController@login');
            $this->get('/admin/logout', 'AdminAuth\AuthController@logout');
	
            $this->get('/admin/register', 'AdminAuth\AuthController@showRegistrationForm');
            $this->post('/admin/register', 'AdminAuth\AuthController@register');
	
            $this->get('/admin/password/reset/{token?}', 'AdminAuth\PasswordController@showResetForm');
            $this->post('/admin/password/email', 'AdminAuth\PasswordController@sendResetLinkEmail');
            $this->post('/admin/password/reset', 'AdminAuth\PasswordController@reset');
            
            
            
            }); # /ベーシック認証