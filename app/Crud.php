<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crud extends Model
{

    /**
     * モデルと関連しているテーブル
     *
     * @var string
     */
     protected $table = 'crud';

    //挿入できるようにカラムを宣言
   # protected $fillable = ['name','mail','gender','age','pref','birthday','tel'];
}
