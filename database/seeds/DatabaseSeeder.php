<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #$this->call('ArticlesTableSeeder');
		Model::unguard(); // セキュリティー解除
        #$this->call('UsersTableSeeder');
		$this->call('AdminTableSeeder');
		Model::reguard(); // セキュリティーを再設定
    }
}
