<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Defaults
    |--------------------------------------------------------------------------
    |
    | This option controls the default authentication "guard" and password
    | reset options for your application. You may change these defaults
    | as required, but they're a perfect start for most applications.
    |
    */

    'defaults' => [
        'guard' => 'web',
        'passwords' => 'users',
    ],

    /*
    |--------------------------------------------------------------------------
    | Authentication Guards
    |--------------------------------------------------------------------------
    |
    | ユーザー認証の種別を定義
    | 　web->ユーザー用
    | 　admin->管理者用
    |
    */

    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'token',
            'provider' => 'users',
        ],
				
	//追加 管理者用
	'admin' => [
            'driver' => 'session',
            'provider' => 'admins',
	],
    ],

    /*
    |--------------------------------------------------------------------------
    | User Providers
    |--------------------------------------------------------------------------
    |
    | 使用するモデルを定義
    |
    */

    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => App\User::class,
        ],

	//追加 管理者用
         'admins' => [
             'driver' => 'eloquent',
             'model' => App\Admin::class,
         ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Resetting Passwords
    |--------------------------------------------------------------------------
    |
    | パスワードリセットの種別を定義
    | provider -> 上述した通り、使用するモデル
    | email -> 再設定メールの本文のviewファイルを指定
    | table -> パスワードリセットに使用するテーブル
    | expaire -> メールを受け取ってから、パスワード再設定可能な時間（分）
    |
    */

    'passwords' => [
        'users' => [
            'provider' => 'users',
            'email' => 'auth.emails.password',
            'table' => 'password_resets',
            'expire' => 60,
        ],
		
	//追加(管理者用)
        'admins' => [
            'provider' => 'admins',
            'email' => 'adminAuth.emails.password',
            'table' => 'password_resets',
            'expire' => 60,
        ],
    ],
];
