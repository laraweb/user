{{--

ユーザーHOME

--}}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-info">
                <div class="panel-heading">ダッシュボード(ユーザー側)</div>

                <div class="panel-body">
                    ユーザー側でログイン成功！
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
