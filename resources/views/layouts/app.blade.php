{{--

ユーザー・管理者　共通レイアウト

--}}
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel5.2 マルチ認証</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/multi_auth') }}">
                    Laravel5.2 マルチ認証
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
						<li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                ユーザー画面 <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
							    <li><a href="{{ url('/home') }}"><i class="fa fa-home" aria-hidden="true"></i>HOME</a></li>
                                <li><a href="{{ url('/login') }}"><i class="fa fa-btn fa-sign-in"></i>ログイン</a></li>
                                <li><a href="{{ url('/register') }}"><i class="fa fa-registered" aria-hidden="true"></i>登録</a></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>ログアウト</a></li>
                            </ul>
						</li>
						
						<li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                管理者画面 <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
							    <li><a href="{{ url('/admin/home') }}"><i class="fa fa-home" aria-hidden="true"></i>HOME</a></li>
                                <li><a href="{{ url('/admin/login') }}"><i class="fa fa-btn fa-sign-in"></i>ログイン</a></li>
                                <li><a href="{{ url('/admin/register') }}"><i class="fa fa-registered" aria-hidden="true"></i>登録</a></li>
                                <li><a href="{{ url('/admin/logout') }}"><i class="fa fa-btn fa-sign-out"></i>ログアウト</a></li>
                            </ul>
						</li>
                    
                    @if ( Auth::check() )
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }}さん
                            </a>
                        </li>
                    @endif
                    
                    @if ( Auth::guard('admin')->check() )
                         <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
							    {{ Auth::guard('admin')->user()->name }}さん
							</a>
						</li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
