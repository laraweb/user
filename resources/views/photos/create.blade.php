@extends('layouts.master_upload')
@section('title', 'ファイルアップロード機能')
@section('content')

@if (session('status'))
<div class="alert alert-success" role="alert" onclick="this.classList.add('hidden')">{{ session('status') }}</div>
@endif

<form method="POST" action="{{ url('/') }}/photos/store" accept-charset="UTF-8" enctype="multipart/form-data" class="form-inline" style="margin-bottom: 30px;">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="imagePreview"></div>
    <div class="input-group">
        <label class="input-group-btn">
            <span class="btn btn-primary">
                   Choose File<input type="file" name="fileName" style="display:none" class="uploadFile">
            </span>
        </label>
        <input type="text" name="fileName" class="form-control" readonly="">
    </div>
<input type="submit" value="アップロードする" class="btn btn-primary">
</form>


@foreach ($photos as $photo)
<div class="panel panel-default">
    <div class="panel-heading">アップロードした日付：{{$photo->created_at}}</div>
    <!-- List group -->
    <ul class="list-group">
        <li class="list-group-item"><img src="{{$photo->path}}"></li>
    </ul>
</div>
@endforeach

@endsection