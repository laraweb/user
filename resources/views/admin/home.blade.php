{{--

管理者HOME

--}}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-danger">
                <div class="panel-heading">ダッシュボード(管理者)</div>
                <div class="panel-body">
				    管理者側でログイン成功！
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
