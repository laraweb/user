@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Laravel5.2 マルチ認証</div>

                <div class="panel-body">
                                        <h2>DBテーブル</h2>
                                        <ul>
                                            <li>ユーザ・・・users(デフォルトで用意されているものを使います)</li>
                                            <li>管理者・・・admins(usersと同構造のものを作成します)</li>
                                        </ul>
					<h2>URI</h2>
                                        <h3>共通</h3>
                                        <ul>
                                            <li>HOME画面・・・{プロジェクト名}/multi_auth</li>
                                            <li>ログアウト画面(= HOME画面)・・・{プロジェクト名}/multi_auth</li>
                                        </ul>
					<h3>管理画面</h3>
					<ul>
                                            <li>ログイン画面・・・{プロジェクト名}/admin/login</li>
                                            <li>登録画面・・・{プロジェクト名}/admin/register</li>
                                            <li>ログイン後の画面・・・{プロジェクト名}/admin/home</li>
					</ul>
					<h3>ユーザ画面</h3>
					<ul>
                                            <li>ログイン画面・・・{プロジェクト名}/login</li>
                                            <li>登録画面・・・{プロジェクト名}/register</li>
                                            <li>ログイン後の画面・・・{プロジェクト名}/home</li>
					</ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
